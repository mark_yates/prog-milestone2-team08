﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program
{
    class program
    {
        static void dateDays()
        {
            var today = DateTime.Today;

            Console.WriteLine("Please enter your date of birth (dd-mm-yyyy): ");
            var dob = Console.ReadLine();

            DateTime birthDate = DateTime.Parse(dob);
            
            TimeSpan elapsed = today.Subtract(birthDate);

            double daysAgo = elapsed.TotalDays;
            Console.WriteLine($"You're {daysAgo} days old.");
        }
        static void dateYears()
        {
            Console.WriteLine("Please enter your age (e.g. 26): ");
            var ansYears = int.Parse(Console.ReadLine());

            DateTime current = DateTime.Today;
            var a = current.AddYears(-ansYears);
                                    
            TimeSpan elapsed = current.Subtract(a);

            double days = elapsed.TotalDays;
            Console.WriteLine($"There are {days} days in {ansYears} years.");
        }
        static List<int> result = new List<int>();
        static void random()
        {

            try
            {
                int score = 0;
                int i = 0;

                do
                {

                    int number = 0;

                    Random rnd = new Random();
                    int Value = rnd.Next(1, 6);

                    Console.WriteLine("--------------------------------");
                    Console.WriteLine($"Your Current Score Is {score}");
                    Console.WriteLine("--------------------------------");
                    Console.WriteLine("Please guess a number between 1-5");

                    string text1 = "x";
                    int num1;

                    bool number1 = int.TryParse(text1, out num1);
                    number = int.Parse(Console.ReadLine());
                    Console.WriteLine($"You entered {number} and the number was {Value}");

                    Console.Clear();
                    if (number1 == false)
                    {

                        if
                            (number == Value)
                            score++;
                        i++;
                    }

                } while (i <= 4);


                result.Add(score);
                Console.Clear();
                Console.WriteLine($"Well done you made it to the end and you got {score} out of 5 ");
                Console.WriteLine("-----------------------------------------------------------------");

                results();
                finish();
            }
            catch
            {

            }

        }

        static void finish()
        {
            Console.WriteLine("-----------------------------------------------------------------");
            Console.WriteLine("Would you like to play again (y/n)");
            string question = Console.ReadLine();

            if (question.Equals("y"))
            {
                Console.Clear();

                random();

            }
            else if (question.Equals("n"))
            {
                Console.WriteLine("Why you no stay for more games");

            }

        }
        static void results()
        {
            foreach (int i in result)
            {
                Console.WriteLine($"Your previous score was [{i}]");
            }

        }


        static void Main(string[] args)
        {
            var ansMenu = 0;
            var returnMenu = "";

            try
            {
                do
                {
                    Console.WriteLine("-- menu   -----");
                    Console.WriteLine("1. Date   -----");
                    Console.WriteLine("2. opt2   -----");
                    Console.WriteLine("3. Game   -----");
                    Console.WriteLine("4. Foods  -----");
                    Console.WriteLine("---------------");
                    Console.WriteLine();
                    Console.WriteLine("Please enter a selection: ");
                    ansMenu = int.Parse(Console.ReadLine());
                    Console.Clear();

                    switch (ansMenu)
                    {
                        case 1:
                            Console.WriteLine("-- Age thing menu --");
                            Console.WriteLine("1. days    ---------");
                            Console.WriteLine("2. years   ---------");
                            Console.WriteLine("--------------------");
                            Console.WriteLine("Please enter a selection: ");
                            var ansDate = int.Parse(Console.ReadLine());
                            Console.Clear();

                            switch (ansDate)
                            {
                                case 1:
                                    dateDays();
                                    break;
                                case 2:
                                    dateYears();
                                    break;
                            }

                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                        case 2:
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                        case 3:
                            random();
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            foodMenu();
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                        default:
                            Console.WriteLine("Please enter a selection.");
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                    }

                } while (returnMenu == "m" || returnMenu == "M");
            }
            catch
            {

            }
        }

        static void foodMenu()
        {
            var submenu = "";

            do

            {                
                Console.Clear();
                Console.WriteLine("      Food menu      ");
                Console.WriteLine("                     ");
                Console.WriteLine("-- 1. Enter foods  --");
                Console.WriteLine("-- 2. Show list    --");
                Console.WriteLine("-- 3. change       --");
                Console.WriteLine("-- 4. End          --\n");
                Console.WriteLine("");
                Console.WriteLine("Please select a number");
                
                
                String input = Console.ReadLine();
                int selectedOption;
                
                if (int.TryParse(input, out selectedOption))

                {
                    switch (selectedOption)
                    {
                        case 1:
                            foods();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        case 2:
                            show(food);
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        case 3:
                            change();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        case 4:
                            end();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                    }

                }

            } while (submenu == "r" || submenu == "R");
            
            }

        static Dictionary<int, string> food = new Dictionary<int, string>();

        static Dictionary<int, string> foods()
        {            
            Console.WriteLine("Please enter 5 of your favorite foods in order of favorite to least favorite.\n");
            
            check();
           
            return food;
        }

        static void show(Dictionary<int,string> food)
        {
               Console.WriteLine("Your favorite {0} foods in order are:  \n",food.Count);
            
            foreach (KeyValuePair<int, string> f in food)
            {
                Console.WriteLine(f);
            }
        }
        
        static void change()
        {
            try { 
            var fooditem = 0;

            show(food);
            
            Console.WriteLine("What item of food would you like to change? (Select the number, press enter then type in the new item\n)");            
            fooditem = int.Parse(Console.ReadLine());
            

            
                    if (fooditem == 1)
                    {
                        food.Remove(1);
                        food.Add(1, Console.ReadLine());
                
                    }
                    if (fooditem == 2)
                    {
                        food.Remove(2);
                        food.Add(2, Console.ReadLine());
                    }
                    if (fooditem == 3)
                    {
                        food.Remove(3);
                        food.Add(3, Console.ReadLine());
                    }
                    if (fooditem == 4)
                    {
                        food.Remove(4);
                        food.Add(4, Console.ReadLine());
                    }
                    if (fooditem == 5)
                    {
                        food.Remove(5);
                        food.Add(5, Console.ReadLine());
            }
                    

            show(food);

            
        }
            catch
            {

            }
        }
         static void check()
        {
            string input = "";
            int value = 1;

            while (food.Count != 5)
            {
                input = Console.ReadLine();

                if (food.Count == 0)
                {
                    food.Add(value, input);
                }
                else
                {
                    if (food.ContainsValue(input))
                    {
                        Console.WriteLine("That is already there");
                    }
                    else
                    {
                        food.Add(value, input);
                        Console.WriteLine("Food Added");
                    }

                }

                value++;

            }
        }
        
        static void end()
        {
            Console.WriteLine("Thanks for taking part press Enter for the menu.\n");
        }
    }
}